Remove Posts Over Word Limit
============================

OAuth Version for Python3
-------------------------

The remove_post_over_word_limit.py is built for python3 and uses OAuth instead of 
the old Praw login as that will soon be deprecated. Instead of python2 this 
requires python3, praw and the [prawoauthutil-2](https://github.com/SmBe19/praw-OAuth2Util) built by [/u/SmBe19](https://www.reddit.com/user/SmBe19).

To get this setup install praw and praw-oauth2util:

    pip install praw
    pip install praw-oauth2util

Then follow the instructions for setting up a reddit app [here](https://github.com/SmBe19/praw-OAuth2Util/blob/master/OAuth2Util/README.md#reddit-config).

And then add your app_key and app_secret to the oauth.txt file. 


Config
------

Set the USERNAME variable to the user you want the bot to send a message to if it crashes. You can comment out the exit(1), but
it may end up in a loop of errors and sending the user a bunch of messages, so be careful.

Set SUBREDDIT_NAME to the name of the subreddit you want it to look in, do not include the /r/. 

Adjust LIMIT to the word limit of you choice.
import re
import time
import praw
import requests
import OAuth2Util

USERNAME = ""
SUBREDDIT_NAME = ""
LIMIT = 500
COMMENT = """
Sorry, your story has been removed for breaking rule 1. I am a bot.
This action was performed automatically.
If you have any issues or doubts, message the moderators.
"""


def count_words_in_post(submission_text):
    return len(re.findall(r'\w+', submission_text))


def scan_submissions(subreddit):
    print("Finding posts...")
    submissions = subreddit.get_new()
    for submission in submissions:
        if count_words_in_post(submission.selftext) > LIMIT:
            remove_post(submission)


def remove_post(submission):
    print("Removing post...")
    comment_post(submission)
    submission.remove()


def comment_post(submission):
    submission.add_comment(COMMENT)


def main():
    print("Logging in...")
    r = praw.Reddit("Remove_Posts_Over_Word_Limit v1.0 /u/EDGYALLCAPSUSERNAME")
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    subreddit = r.get_subreddit(SUBREDDIT_NAME)

    while True:
        try:
            # refresh the token if neccessary
            o.refresh()
            scan_submissions(subreddit)
        # Catches if reddit goes down
        except requests.exceptions.ConnectionError as e:
            print("Error: Reddit is down, sleeping...")
            time.sleep(200)  # sleep because reddit is down
        # And a catch all, sends message and exits
        except Exception as e:
            # Send yourself a message if bot goes down
            # With a reason
            r.send_message(USERNAME,
                           "Hey, I went down",
                           "Here's why ```{}```\n\n".format(e),
                           captcha=None)
            # Comment out if you want it to continue
            # Instead of exiting upon error
            exit(1)

        # wait 5 minutes to not go over request limit
        print("Sleeping...")
        time.sleep(300)


if __name__ == '__main__':
    main()
